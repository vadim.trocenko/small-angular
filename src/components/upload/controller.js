function UploadFormController(api) {
  this.file = api.file;

  this.uploadFile = () => {
    const form = new FormData();
    form.append('sampleFile', this.file);
    api.uploadFile(form);
  };
}

UploadFormController.dependencies = ['api'];
export default UploadFormController;
