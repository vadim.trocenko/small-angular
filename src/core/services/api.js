function api($http) {
  const file = {};
  const fileList = [];

  function getFileList() {
    return fileList;
  }

  function loadFileList() {
    $http.get('/list', { responseType: 'json' })
      .then(res => {
        fileList.length = 0;
        fileList.push(...res);
      });
  }

  function removeFile(url) {
    const files = fileList.filter(file => file.name === url);

    fileList.splice(fileList.indexOf(...files), 1);
    $http.delete(`/list/${url}`, { responseType: 'text' }).catch(() => {
      fileList.push(...files);
    });
  }

  function downloadFileToLocal(url, file) {
    const downloadLink = document.createElement('a');

    downloadLink.href = url;
    downloadLink.download = file;
    downloadLink.click();
  }

  function setSelected(value) {
    file.fileName = value;
  }

  function downloadFile(path, config) {
    $http.get(`files/${path}`, { responseType: 'blob', ...config })
      .then(res => {
        const url = URL.createObjectURL(res);

        if (res.type.includes('image')) {
          file.url = url;
          return;
        }

        downloadFileToLocal(url, path);
      });
  }

  function uploadFile(data, config) {
    $http.post('upload/', data, { responseType: 'blob', ...config })
      .then(() => loadFileList());
  }


  return { downloadFile, uploadFile, setSelected, getFileList, loadFileList, removeFile, file };
}

api.dependencies = ['$http'];
export default api;
