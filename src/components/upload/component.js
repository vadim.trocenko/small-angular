import UploadFormController from './controller.js';
import template from './template.html?raw';
function uploadForm() {
  return {
    controller: UploadFormController.name,
    controllerAs: 'uploadController',
    template
  };
}

export default uploadForm;
