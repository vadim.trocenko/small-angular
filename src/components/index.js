export { default as initFilesList } from './filesList/index.js';
export { default as initUploadForm } from './upload/index.js';
export { default as initDownloadForm } from './download/index.js';
export { default as initPicture } from './picture/index.js';
