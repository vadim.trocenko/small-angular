import component from './component.js';
import controller from './controller.js';
import './style.css';

function initPicture(app) {
  app
    .controller(controller.name, controller)
    .component('picture', component);
}

export default initPicture;
