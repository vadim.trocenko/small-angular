import DownloadFormController from './controller.js';
import downloadForm from './component.js';
import './style.css';

function initDownloadForm(app) {
  app
    .controller(DownloadFormController.name, DownloadFormController)
    .component('download-form', downloadForm);
}

export default initDownloadForm;
