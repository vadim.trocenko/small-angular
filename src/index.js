import angular from 'angular-lib';
import * as components from '@components/index.js';
import * as services from '@services/index.js';
import * as configs from '@config/index.js';
import './style.css';

const app = angular.module('myApp', []);

Object.entries(services).forEach(services => app.service(...services));
Object.values(configs).forEach(configs => app.config(configs));
Object.values(components).forEach(comp => comp(app));
