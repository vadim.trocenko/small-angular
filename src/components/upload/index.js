import UploadFormController from './controller.js';
import uploadForm from './component.js';
import './style.css';

function initUploadForm(app) {
  app
    .controller(UploadFormController.name, UploadFormController)
    .component('upload-form', uploadForm);
}

export default initUploadForm;
