function pictureController(api) {
  this.file = api.file;
}

pictureController.dependencies = ['api'];
export default pictureController;
