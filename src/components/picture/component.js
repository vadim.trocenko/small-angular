import PictureController from './controller.js';
import template from './template.html?raw';
function picture() {
  return {
    controller: PictureController.name,
    controllerAs: 'pictureController',
    template
  };
}

export default picture;
