export default function configHttp($http) {
  $http.url = 'http://localhost:8000/';
}

configHttp.dependencies = ['$http'];
