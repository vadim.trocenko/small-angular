import FilesListController from './controller.js';
import template from './template.html?raw';
function filesListComponent() {
  return {
    controller: FilesListController.name,
    controllerAs: 'filesList',
    template
  };
}

export default filesListComponent;
