import FilesListController from './controller.js';
import filesListComponent from './component.js';
import './style.css';

function initFilesList(app) {
  app
    .controller(FilesListController.name, FilesListController)
    .component('files-list-component', filesListComponent);
}

export default initFilesList;
