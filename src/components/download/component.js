import DownloadFormController from './controller.js';
import template from './template.html?raw';
function downloadForm() {
  return {
    controller: DownloadFormController.name,
    controllerAs: 'downloadController',
    template
  };
}

export default downloadForm;
