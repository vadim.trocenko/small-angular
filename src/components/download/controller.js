function DownloadFormController(api) {
  this.file = api.file;

  this.downloadFile = () => {
    const { fileName } = this.file;

    if (!fileName) {
      return;
    }

    api.downloadFile(fileName);
  };
}

DownloadFormController.dependencies = ['api'];
export default DownloadFormController;
