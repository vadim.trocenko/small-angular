import { defineConfig } from 'vite';

const { resolve } = require('path');

import handlebars from 'vite-plugin-handlebars';
import htmlVitePlugin from 'vite-plugin-html';
import viteCompression from 'vite-plugin-compression';

const mode = process.env.NODE_ENV || 'development';

const config = {
  mode,
  plugins: [
    handlebars({
      context: {
        title: mode === 'production' ? 'app-prod' : 'app-dev'
      }
    }),
    htmlVitePlugin({
      minify: mode === 'production'
    })
  ],
  resolve: {
    alias: {
      '@components': resolve(__dirname, 'src/components'),
      '@config': resolve(__dirname, 'src/config'),
      '@utils': resolve(__dirname, 'src/utils'),
      '@services': resolve(__dirname, 'src/core/services')
    }
  },
  build: {
    sourcemap: true,
    outDir: resolve(__dirname, 'dist'),
    rollupOptions: {
      output: {
        assetFileNames: assetInfo => {
          let [, ext] = assetInfo.name.split('.');

          if ((/png|jpe?g|svg|gif/i).test(ext)) {
            ext = 'img';
          }
          return `${ext}/[name][hash][extname]`;
        },
        entryFileNames: 'js/[name][hash].js'
      }
    }
  }
};

if (mode === 'production') {
  config.plugins.push(viteCompression());
}

export default defineConfig(config);
