function FilesListController(api) {
  api.loadFileList();

  this.file = api.file;
  this.fileList = api.getFileList();
  this.removeFile = api.removeFile;
  this.selectItem = api.setSelected;
}

FilesListController.dependencies = ['api'];
export default FilesListController;
